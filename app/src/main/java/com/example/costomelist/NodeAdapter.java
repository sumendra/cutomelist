package com.example.costomelist;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.costomelist.Note;
import com.example.costomelist.NoteAddUpdateActivity;
import com.example.costomelist.R;

import java.util.ArrayList;

import static com.example.costomelist.Db.DatabaseContract.NoteColumns.CONTENT_URI;

public class NodeAdapter extends RecyclerView.Adapter<NodeAdapter.NoteHolder> {

    private ArrayList<Note> list= new ArrayList<>();
    private Activity activity;

    public NodeAdapter(Activity activity){
        this.activity= activity;
    }

    public ArrayList<Note> getListNotes(){
        return list;
    }

    // mengatur list note
    public void setListNotes(ArrayList<Note> listNotes){
        if(listNotes.size() > 0){
            this.list.clear();
        }
        this.list.addAll(listNotes);
        notifyDataSetChanged();
    }

    public void addItem(Note note){
        this.list.add(note);
        notifyItemInserted(list.size() -1);
    }

    // update data di udapter

    public void updateItem(int position, Note note){
        this.list.set(position, note);
        notifyItemChanged(position, note);
    }

    // hapus data dari adapter
    public void removeItem(int position){
        this.list.remove(position);
        notifyItemRangeChanged(position, list.size());
        notifyItemRemoved(position);
    }

    @NonNull
    @Override
    public NoteHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_note, parent, false);
        return new NoteHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NoteHolder holder, int position) {
        holder.textTitle.setText(getListNotes().get(position).getTitle());
        holder.textDeskripsi.setText(getListNotes().get(position).getDescription());
        holder.textDate.setText(getListNotes().get(position).getDate());

        // ketika intent ke activity note maka kirim position dari adapter untuk mempermudah proses update
        holder.cardNote.setOnClickListener(new CustomOnItemClickListener(position, new CustomOnItemClickListener.OnItemClickCallback() {
            @Override
            public void onItemCliked(View view, int position) {
                Intent intent= new Intent(activity, NoteAddUpdateActivity.class);
                Uri uri= Uri.parse(CONTENT_URI + "/"+ getListNotes().get(position).getId());
                intent.setData(uri);
                intent.putExtra(NoteAddUpdateActivity.EXTRA_POSITION, position);
                intent.putExtra(NoteAddUpdateActivity.EXTRA_NOTE, list.get(position));
                activity.startActivityForResult(intent, NoteAddUpdateActivity.REQUEST_UPDATE);
            }
        }));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class NoteHolder extends RecyclerView.ViewHolder {
        final TextView textTitle;
        TextView textDeskripsi;
        TextView textDate;
        CardView cardNote;
        public NoteHolder(@NonNull View itemView) {
            super(itemView);
            textTitle= itemView.findViewById(R.id.text_item_title);
            textDeskripsi= itemView.findViewById(R.id.text_item_deskription);
            textDate= itemView.findViewById(R.id.text_item_Date);
            cardNote= itemView.findViewById(R.id.card_item_note);
        }
    }
}
