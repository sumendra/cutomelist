package com.example.costomelist;

import android.database.Cursor;

public interface LoadNotesCallback {
    void postExecute(Cursor note);
}
