package com.example.costomelist;

import android.view.View;
import android.widget.AdapterView;

public class CustomOnItemClickListener implements View.OnClickListener{
    private int position;
    private OnItemClickCallback onItemClickCallback;
    public CustomOnItemClickListener(int position, OnItemClickCallback  onItemClickCallback){
        this.position= position;
        this.onItemClickCallback= onItemClickCallback;
    };

    @Override
    public void onClick(View view) {
        onItemClickCallback.onItemCliked(view, position);
    }

    public interface OnItemClickCallback{
        void onItemCliked(View view, int position);
    }
}
